package com.kmarinos.amzmockcustomer;

import com.kmarinos.amzmockcustomer.model.Customer;
import com.kmarinos.amzmockcustomer.model.Product;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;
import me.tongfei.progressbar.ProgressBarStyle;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication
@RequiredArgsConstructor
public class AmzMockCustomerApplication {

	private final AmzStoreClient client;

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(AmzMockCustomerApplication.class, args);
		SpringApplication.exit(ctx,()->0);
	}

	@Bean
	public CommandLineRunner run(){
		return args -> {
			Random random = new Random();
			new Thread(() ->{
				for(int i=0;i<10;i++){
					createCustomers(1000);
					try {
						Thread.sleep(random.nextInt(30000)+30000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}).start();
			new Thread(() ->{
				for(int i=0;i<10;i++){
					placeOrders(200);
					try {
						Thread.sleep(random.nextInt(30000)+30000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}).start();
		};
	}
	public void createCustomers(int amount){
		IntStream stream =IntStream.range(0,amount);
		ProgressBar.wrap(stream,new ProgressBarBuilder().setStyle(ProgressBarStyle.ASCII).setTaskName("Create customers").showSpeed()).forEach(i->{
			client.registerNewCustomer();
		});
	}
	public void placeOrders(int amount){
		Random r = new Random();
		IntStream stream =IntStream.range(0,amount);
		ProgressBar.wrap(stream,new ProgressBarBuilder().setStyle(ProgressBarStyle.ASCII).setTaskName("Create orders").showSpeed()).forEach(i->{
			placeOrder(client::login,()->r.nextInt(3)+1,()->r.nextInt(10)+1);
		});
	}
	public void placeOrder(Supplier<Customer> customerSupplier, Supplier<Integer> amountOfProducts,Supplier<Integer> quantity){
		Random r = new Random();

		//get random Customer
		Customer customer = customerSupplier.get();
		//get all categories
		final var allCategories = client.getAllCategories();
		//browse for products in a random category
		List<Product> products =client.browseInCategory(pickCategory(pickRandomPosition(r,allCategories.size()),allCategories));
		//Pick products and quantity
		Map<Product,Integer> itemsMap= pickSomeProducts(
				r,
				amountOfProducts,
				products)
				.stream().collect(Collectors.toMap(Function.identity(),p->quantity.get()));
		//place order
		client.placeOrder(customer,itemsMap, LocalDateTime.now());
	}

	private List<Product> pickSomeProducts(Random r,Supplier<Integer>amountProvider, List<Product> products){
		int count=0;
		int amount = amountProvider.get();
		List<Product> picked = new ArrayList<>();

		while(count<amount){
			if(!products.isEmpty()){
				picked.add(pickProduct(pickRandomPosition(r,products.size()),products));
			}
			count++;
		}
		return picked;
	}

	private Supplier<Integer> pickRandomPosition(Random r,int total){
		return ()->r.nextInt(total);
	}


	private String pickCategory(Supplier<Integer> positionSupplier,List<String> categories){
		return categories.get(positionSupplier.get());
	}
	private Product pickProduct(Supplier<Integer> positionSupplier,List<Product> products){
		return products.remove(positionSupplier.get().intValue());
	}
}
