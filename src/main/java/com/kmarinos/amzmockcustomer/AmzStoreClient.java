package com.kmarinos.amzmockcustomer;

import com.github.javafaker.Faker;
import com.kmarinos.amzmockcustomer.model.Address;
import com.kmarinos.amzmockcustomer.model.Customer;
import com.kmarinos.amzmockcustomer.model.CustomerPOST;
import com.kmarinos.amzmockcustomer.model.Product;
import com.kmarinos.amzmockcustomer.model.SalesOrder;
import com.kmarinos.amzmockcustomer.model.SalesOrderItem;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import javax.net.ssl.SSLException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;
import reactor.netty.tcp.TcpClient;

@Component
@Slf4j
public class AmzStoreClient {

  private final WebClient client;
  private final Faker faker;

  public AmzStoreClient(WebClient.Builder builder) throws SSLException {
    SslContext context = SslContextBuilder.forClient()
        .trustManager(InsecureTrustManagerFactory.INSTANCE)
        .build();

    var tcpClient = TcpClient.create().secure ( sslProviderBuilder -> sslProviderBuilder.sslContext(context) );
    var httpClient = HttpClient.from(tcpClient);
    faker = new Faker(new Locale("de-DE"));
    this.client = builder.baseUrl("http://localhost:8081/api").clientConnector(new ReactorClientHttpConnector(httpClient)).build();
  }

  public void placeOrder(Customer customer,Map<Product,Integer>itemsMap, LocalDateTime forDate){
    List<SalesOrderItem> itemsList = new ArrayList<>();
    itemsMap.forEach((key,value)->{
        itemsList.add(SalesOrderItem.builder()
            .product(key)
            .quantity(value)
            .build());
    });
    SalesOrder order = SalesOrder.builder()
        .items(itemsList)
        .submitOrderAt(forDate)
        .build();
    client.post().uri("/customers/"+customer.getId()+"/buy")
        .body(BodyInserters.fromValue(order))
        .retrieve().bodyToMono(SalesOrder.class).block();
  }

  public List<Product> browseInCategory(String category){
    return client.get().uri("/browse?c="+category).accept(MediaType.APPLICATION_JSON)
        .retrieve()
        .bodyToFlux(Product.class)
        .collectList().block();
  }

  public List<String> getAllCategories(){
    return Arrays.asList(client.get().uri("/categories").accept(MediaType.APPLICATION_JSON)
        .retrieve()
        .bodyToMono(String[].class)
        .block());
  }

  public Customer login(){
    return client.get().uri("/login").accept(MediaType.APPLICATION_JSON).retrieve().bodyToMono(Customer.class).block();
  }

  public void registerNewCustomer(){
    Customer registeredCustomer = client.post().uri("/customers").accept(MediaType.APPLICATION_JSON)
        .body(BodyInserters.fromValue(generateRandomCustomer()))
        .retrieve()
        .bodyToMono(Customer.class).block();
  }

  public void getAllCustomers() {
    List<Customer> customers =
    client
        .get()
        .uri("/customers")
        .accept(MediaType.APPLICATION_JSON)
        .retrieve()
        .bodyToFlux(Customer.class)
        .collectList().block();

    System.out.println("Found clients:");
    assert customers != null;
    customers.forEach(c -> System.out.println(c.getEmail()));
  }
  private CustomerPOST generateRandomCustomer(){

    Address address1 =
        Address.builder()
            .city(faker.address().city())
            .countryCode(faker.address().countryCode())
            .deliveryAddress(true)
            .primaryAddress(true)
            .invoiceAddress(true)
            .street(faker.address().streetName())
            .streetNumber(faker.address().streetAddressNumber())
            .zipCode(faker.address().zipCode())
            .state(faker.address().state())
            .build();
    return
        CustomerPOST.builder()
            .firstname(faker.name().firstName())
            .lastname(faker.name().lastName())
            .username(faker.name().username())
            .email(faker.internet().safeEmailAddress())
            .address(address1)
            .build();
  }
}
