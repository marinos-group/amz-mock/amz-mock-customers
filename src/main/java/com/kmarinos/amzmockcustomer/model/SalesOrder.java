package com.kmarinos.amzmockcustomer.model;

import java.time.LocalDateTime;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SalesOrder {

  LocalDateTime submitOrderAt;
  List<SalesOrderItem> items;

}
