package com.kmarinos.amzmockcustomer.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Customer {

  String id;
  String firstname;
  String lastname;
  String username;
  String email;
  List<Address> addresses;
}
