package com.kmarinos.amzmockcustomer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Address {

  String id;
  boolean deliveryAddress;
  boolean invoiceAddress;
  boolean primaryAddress;
  String street;
  String streetNumber;
  String zipCode;
  String city;
  String state;
  String countryCode;
}
